FROM node:iron-alpine AS build

ENV NODE_ENV=production
COPY ./package*.json /build/
WORKDIR /build
RUN npm ci

FROM node:22.11.0-alpine

ENV NODE_ENV=production
USER node
WORKDIR /app
COPY --chown=node:node --from=build /build/node_modules /app/node_modules
COPY --chown=node:node ./ /app/
# Disable TypeScript type checks on container builds for faster startup time
RUN sed -i 's/ignoreBuildErrors\: false/ignoreBuildErrors\: true/g' next.config.js

EXPOSE 3000

CMD npm run build && npm run start:prod
